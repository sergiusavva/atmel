<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if ($rows): ?>
  <?php foreach ($rows as $id => $row): ?>
    <div class="views-row-floated">
      <?php print $row; ?>
      <?php if ($id < count($rows) - 1) : ?>
        <?php print '<div class="tags-comma">,&nbsp;</div>'; ?>
      <?php endif; ?>
    </div>
  <?php endforeach; ?>
<?php endif; ?>
