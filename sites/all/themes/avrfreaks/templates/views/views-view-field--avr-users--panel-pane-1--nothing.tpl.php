<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */

$total_comments = db_query("SELECT count(*) FROM {comment}")->fetchField();
$user_comments = db_query("SELECT count(*) FROM {comment} WHERE uid = :uid", array(":uid" => $view->result[0]->uid))->fetchField();

$percentage = number_format((($user_comments * 100)/$total_comments), 2, '.', '');

$user_join_time = $view->result[0]->_field_data['uid']['entity']->created;
$current_time = time();

function dateDiffTs($start_ts, $end_ts) {
  $diff = $end_ts - $start_ts;
  return round($diff / 86400);
}

$account = user_load($view->result[0]->uid);
$level = avrfreaks_rank_get_user_rank($user_comments, $account);

$user_days_since_registered = dateDiffTs($user_join_time, $current_time);

$posts_per_day = number_format(($user_comments/$user_days_since_registered), 2, '.', '');

$output = $user_comments . '<br/>[' . $percentage . '% of total / ' . $posts_per_day . ' posts per day]</br>' . '<span class="views-label">Level: </span>' . $level;

print $output;
