<?php

/**
 * @file
 *
 * Theme implementation: Template for each forum post whether node or comment.
 *
 * All variables available in comment.tpl.php are available here. 
 * In addition, AVRFreaks Forum makes available the following variables:
 *
 * - $reply_link: Text link / button to reply to topic.
 * - $total_posts: Number of posts in topic (not counting first post).
 * - $new_posts: Number of new posts in topic, and link to first new.
 * - $links_array: Unformatted array of links.
 * - $account: User object of the post author.
 * - $name: User name of post author.
 * - $author_pane: Entire contents of the Author Pane template.
 */

?>
<div id="<?php print $post_id; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="forum-post-info clearfix">
    <?php if (!empty($in_reply_to)): ?>
   	 <span class="forum-in-reply-to"><?php print $in_reply_to; ?></span>
    <?php endif; ?>

<!--    <span class="forum-post-number">--><?php //print $permalink; ?><!--</span>-->
    <div class="forum-author-name-block">
      <div class="author-pane user-picture-available">
        <?php if (!empty($author)): ?>
          <?php print $author; ?>
        <?php endif; ?>
        <div class="online-status">
          <?php print $online; ?>
        </div>
        <div class="user-picture">
          <?php if (!empty($picture)): ?>
            <?php print $picture; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="forum-post-panel-sub">
      <div class="forum-post-user-rate">
          <?php if (!empty($user_rate)): ?>
          <?php print $user_rate; ?>
          <?php endif; ?>
      </div>
      <div class="forum-post-panel-joined">
          <?php if (!empty($join_date)): ?>
          <?php print $join_date; ?>
          <?php endif; ?>
      </div>
      <div class="forum-post-panel-posts">
          <?php if (!empty($user_posts)): ?>
          <?php print $user_posts; ?>
          <?php endif; ?>
      </div>
      <div class="forum-post-user-location">
          <?php if (!empty($user_location)): ?>
          <?php print $user_location; ?>
          <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="forum-post-wrapper">
    <div class="forum-post-panel-main clearfix">
        <div class="forum-post-upper-panel">
            <div class="forum-posted-on">
                <span class="label-text">Posted: </span>
                <?php print $created; ?>

                <?php if (!empty($new)): ?>
                <a id="new"><span class="new">(<?php print $new ?>)</span></a>
                <?php endif; ?>

                <?php if ($comment_flagged): ?>
                <a id="comment-flagged"><span class="comment-flagged">(<?php print t('This reply answered the question') ?>)</span></a>
                <?php endif; ?>

                <?php if (!empty($first_new)): ?>
                <?php print $first_new; ?>
                <?php endif; ?>

                <?php if (!empty($new_output)): ?>
                <?php print $new_output; ?>
                <?php endif; ?>
            </div>
        </div>
<!--      --><?php //if (!empty($title)): ?>
<!--        <div class="forum-post-title">-->
<!--          --><?php //print $title ?>
<!--        </div>-->
<!--      --><?php //endif; ?>

      <div class="forum-post-content">
        <?php
          hide($content['taxonomy_forums']);
          hide($content['comments']);
          hide($content['links']);
          hide($content['body']);
          print render($content);
        ?>
      </div>

      <?php if (!empty($post_edited)): ?>
        <div class="post-edited">
          <?php print $post_edited ?>
        </div>
      <?php endif; ?>

      <?php if (!empty($signature)): ?>
        <div class="author-signature">
          <?php print $signature ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <div class="forum-post-footer clearfix">
    <div class="forum-post-links">
      <?php print render($content['links']); ?>
      <a href="#forum-topic-top" title="<?php print t('Jump to top of page'); ?>" class="af-button-small"><span><?php print t("Top"); ?></span></a>
    </div>
  </div>
</div>
<?php print render($content['comments']); ?>
