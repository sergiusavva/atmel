(function ($) {
    Drupal.behaviors.addFlexSlider = {
        attach: function (context, settings) {
            if ($('.flexslider').length) {
                $('.flexslider').flexslider({
                        directionNav: true,
                        animation: "slide",
                        animationLoop: true,
                        minItems: 1,
                        maxItems: 1,
                        move: 1,
                        controlNav: true,
                        slideshow: true
                    }
                );
            }
        }
    };

//    Drupal.behaviors.pjogetPagePlaceholderLabel= {
//      attach: function (context, settings) {
//        $('.page-projects .views-widget-filter-title .form-text').focus(function(){
//          $(this).closest('.views-widget').siblings('label').hide();
//        }).blur(function(){
//          if ($(this).val() == 0) {
//            $(this).closest('.views-widget').siblings('label').show();
//          };
//        })
//      }
//    }
})(jQuery);