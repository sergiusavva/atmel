<?php
/**
 * @file
 * commons_wysiwyg.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function commons_wysiwyg_filter_default_formats() {
  $formats = array();

  // Exported format: Filtered HTML.
  $formats['filtered_html'] = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'cache' => 0,
    'status' => 1,
    'weight' => -9,
    'filters' => array(
      'bbcode' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(
          'bbcode_make_links' => 1,
          'bbcode_filter_nofollow' => 1,
          'bbcode_encode_mailto' => 1,
          'bbcode_paragraph_breaks' => 2,
          'bbcode_debug' => 0,
        ),
      ),
      'freelinking' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'quote' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => 0,
    'status' => 1,
    'weight' => -10,
    'filters' => array(
      'bbcode_quote' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'bbcode' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(
          'bbcode_make_links' => 1,
          'bbcode_filter_nofollow' => 1,
          'bbcode_encode_mailto' => 1,
          'bbcode_paragraph_breaks' => 1,
          'bbcode_debug' => 0,
        ),
      ),
      'quote' => array(
        'weight' => -41,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
