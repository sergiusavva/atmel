<?php
/**
 * @file
 * avrfreaks_subscription.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function avrfreaks_subscription_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function avrfreaks_subscription_flag_default_flags() {
  $flags = array();
  // Exported flag: "Bookmarks".
  $flags['bookmarks'] = array(
    'content_type' => 'node',
    'title' => 'Bookmarks',
    'global' => 0,
    'types' => array(
      0 => 'blog',
      1 => 'forum',
      2 => 'project',
      3 => 'wiki',
    ),
    'flag_short' => 'Bookmark it',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Bookmarked',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 2,
    'module' => 'avrfreaks_subscription',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}
