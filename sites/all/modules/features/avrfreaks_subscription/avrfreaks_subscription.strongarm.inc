<?php
/**
 * @file
 * avrfreaks_subscription.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function avrfreaks_subscription_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'message_subscribe_use_queue';
  $strongarm->value = '1';
  $export['message_subscribe_use_queue'] = $strongarm;

  return $export;
}
