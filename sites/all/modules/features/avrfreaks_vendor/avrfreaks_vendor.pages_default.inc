<?php
/**
 * @file
 * avrfreaks_vendor.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function avrfreaks_vendor_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'vendors';
  $page->task = 'page';
  $page->admin_title = 'Vendors';
  $page->admin_description = 'Vendors page';
  $page->path = 'vendors';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_vendors_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'vendors';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Vendors',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => 'page-projects',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'one';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'one_main' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Vendors';
  $display->uuid = 'aa97427e-7fe0-4555-bc40-a0ff4a96912d';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-670d21a8-baf6-41cc-b62f-e5d1e64d4ce4';
    $pane->panel = 'one_main';
    $pane->type = 'views';
    $pane->subtype = 'vendors_landing';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 1,
      'nodes_per_page' => '12',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-commons-bw-group page-left',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '670d21a8-baf6-41cc-b62f-e5d1e64d4ce4';
    $display->content['new-670d21a8-baf6-41cc-b62f-e5d1e64d4ce4'] = $pane;
    $display->panels['one_main'][0] = 'new-670d21a8-baf6-41cc-b62f-e5d1e64d4ce4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['vendors'] = $page;

  return $pages;

}
