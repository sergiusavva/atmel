<?php
/**
 * @file
 * commons_misc.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function commons_misc_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -98;
  $handler->conf = array(
    'title' => 'Fallback panel',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(
      0 => array(
        'identifier' => 'User from Node (on node.node_author)',
        'keyword' => 'user',
        'name' => 'entity_from_schema:uid-node-user',
        'context' => 'argument_entity_id:node_1',
        'id' => 1,
      ),
    ),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'blog' => 'blog',
              'wiki' => 'wiki',
              'forum' => 'forum',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => TRUE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'two_66_33';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'two_66_33_top' => NULL,
      'two_66_33_first' => NULL,
      'two_66_33_second' => NULL,
      'two_66_33_bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'da99dbdb-5ba9-9ab4-7944-b91485cd4086';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-2ab86e9f-a713-b4c4-518b-5ef5c15675df';
    $pane->panel = 'two_66_33_first';
    $pane->type = 'node_content';
    $pane->subtype = 'node_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'links' => 1,
      'no_extras' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'identifier' => '',
      'link' => 1,
      'leave_node_title' => 0,
      'build_mode' => 'full',
      'context' => 'argument_entity_id:node_1',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-commons-bw-group',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2ab86e9f-a713-b4c4-518b-5ef5c15675df';
    $display->content['new-2ab86e9f-a713-b4c4-518b-5ef5c15675df'] = $pane;
    $display->panels['two_66_33_first'][0] = 'new-2ab86e9f-a713-b4c4-518b-5ef5c15675df';
    $pane = new stdClass();
    $pane->pid = 'new-8543948d-a9a1-4924-41d1-70b706cba765';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'avrfreaks_social_share';
    $pane->subtype = 'avrfreaks_social_share';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'return' => 'Finish',
      'cancel' => 'Cancel',
      'override_title' => 1,
      'override_title_text' => 'Share this post',
      'form_build_id' => 'form-LPJWgF1AxjJnNRRpvpFMQaX4ADjAo9GYXD_0clvO-Jc',
      'form_token' => 'I91XBCcQH65q3AcwSdNhot68t5km-sXZN2Xu_b_mmUw',
      'form_id' => 'avrfreaks_social_avrfreaks_social_share_content_type_edit_form',
      'op' => 'Finish',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8543948d-a9a1-4924-41d1-70b706cba765';
    $display->content['new-8543948d-a9a1-4924-41d1-70b706cba765'] = $pane;
    $display->panels['two_66_33_second'][0] = 'new-8543948d-a9a1-4924-41d1-70b706cba765';
    $pane = new stdClass();
    $pane->pid = 'new-b85802e2-475c-6734-2d9b-a352ac59bd60';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'views_panes';
    $pane->subtype = 'avr_projects-panel_pane_3';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'relationship_entity_from_schema:uid-node-user_1',
      ),
      'override_title' => 1,
      'override_title_text' => 'Other projects by %node:author',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-commons-bw-group',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'b85802e2-475c-6734-2d9b-a352ac59bd60';
    $display->content['new-b85802e2-475c-6734-2d9b-a352ac59bd60'] = $pane;
    $display->panels['two_66_33_second'][1] = 'new-b85802e2-475c-6734-2d9b-a352ac59bd60';
    $pane = new stdClass();
    $pane->pid = 'new-8a00d949-c039-88f4-f58a-8a0b61dc0fff';
    $pane->panel = 'two_66_33_second';
    $pane->type = 'views_panes';
    $pane->subtype = 'avr_followed-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => 'Projects I follow',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-commons-bw-group',
    );
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '8a00d949-c039-88f4-f58a-8a0b61dc0fff';
    $display->content['new-8a00d949-c039-88f4-f58a-8a0b61dc0fff'] = $pane;
    $display->panels['two_66_33_second'][2] = 'new-8a00d949-c039-88f4-f58a-8a0b61dc0fff';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}
