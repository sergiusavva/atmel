<?php

/**
 * Form builder.
 */
function avrfreaks_user_merge_form($form, &$form_state, $account) {
  $form['#account'] = $account;

  $form['email_to_delete'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail to merge'),
    '#description' => t('You will not be able to login with this e-mail anymore!'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Request account merging'),
  );

  return $form;
}

/**
 * Form validate.
 */
function avrfreaks_user_merge_form_validate($form, &$form_state) {
  $user_to_delete = user_load_by_mail($form_state['values']['email_to_delete']);
  if (empty($user_to_delete->uid) || $user_to_delete->uid == 1) {
    form_set_error('email_to_delete', t('Invalid e-mail address.'));
    return;
  }

  $user_to_keep = $form['#account'];
  if ($user_to_keep->uid == $user_to_delete->uid) {
    form_set_error('email_to_delete', t('E-mail must be different than yours.'));
    return;
  }

  $form_state['values']['user_to_keep'] = $user_to_keep;
  $form_state['values']['user_to_delete'] = $user_to_delete;
}

/**
 * Form submit.
 */
function avrfreaks_user_merge_form_submit($form, &$form_state) {
  _avrfreaks_user_merge_transaction_create($form_state['values']['user_to_keep']->uid, $form_state['values']['user_to_delete']->uid);
  _avrfreaks_user_merge_notify($form_state['values']['user_to_keep'], $form_state['values']['user_to_delete']);
  drupal_set_message(t('An e-mail was sent to %mail. Click on the confirmation link within the next 24 hours to confirm the merge', array('%mail' => $form_state['values']['user_to_delete']->mail)));
}

/**
 * Menu callback.
 */
function avrfreaks_user_merge_confirm($account, $timestamp = 0, $hashed_pass = '') {
  // Time out in seconds until cancel URL expires; 24 hours = 86400 seconds.
  $timeout = 86400;
  $current = REQUEST_TIME;

  // Basic validation of arguments.
  if (!empty($timestamp) && !empty($hashed_pass)) {
    // Validate expiration and hashed password/login.
    if ($timestamp <= $current && $current - $timestamp < $timeout && $account->uid && $timestamp >= $account->login && $hashed_pass == user_pass_rehash($account->pass, $timestamp, $account->login)) {
      if (
        ($usermerge_transaction = _avrfreaks_user_merge_transaction_load($account->uid, $timestamp)) &&
        ($user_to_keep = user_load($usermerge_transaction->user_to_keep_uid)) &&
        ($user_to_delete = user_load($usermerge_transaction->user_to_delete_uid))
      ) {
        // Merge accounts
        usermerge_do_the_merge($user_to_delete, $user_to_keep);
        db_delete('avrfreaks_usermerge_transaction')
          ->condition('id', $usermerge_transaction->id, '=')
          ->execute();
        watchdog('avrfreaks_usermerge', 'Accounts merged. User to delete: %user_to_delete; user to keep: %user_to_keep', array('%user_to_delete' => $user_to_delete->mail, '%user_to_keep' => $user_to_keep->mail));

        // User logout
        session_destroy();
        drupal_set_message(t('Account merged successfully! Please login again.'));
        drupal_goto('user');
      }
    }
    else {
      drupal_set_message(t('You have tried to use a account merge link that has expired.'));
      drupal_goto('');
    }
  }
  drupal_access_denied();
}
