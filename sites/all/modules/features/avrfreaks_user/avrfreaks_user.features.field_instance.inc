<?php
/**
 * @file
 * avrfreaks_user.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function avrfreaks_user_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_company'
  $field_instances['user-user-field_company'] = array(
    'bundle' => 'user',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 14,
      ),
    ),
    'display_in_partial_form' => 0,
    'entity_type' => 'user',
    'field_name' => 'field_company',
    'label' => 'Company',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => 0,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 6,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Company');

  return $field_instances;
}
