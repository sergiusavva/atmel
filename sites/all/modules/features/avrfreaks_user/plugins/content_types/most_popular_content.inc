<?php

/**
 * @file
 * Provide a block to extract current node fields.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Most popular content'),
  'description' => t('Show most popular projects/blogs if user has none of his own.'),
  'category' => t('AvrFreaks'),
  'edit form' => 'avrfreaks_ctools_plugins_popular_edit_form',
  'render callback' => 'avrfreaks_ctools_plugins_popular_render',
  'admin info' => 'avrfreaks_ctools_plugins_popular_admin_info',
  'defaults' => array('field' => '')
);

/**
 * 'admin info' callback for panel pane.
 */
function avrfreaks_ctools_plugins_popular_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = '';
    return $block;
  }
}

/**
 * 'Edit form' callback for the content type.
 */
function avrfreaks_ctools_plugins_popular_edit_form($form, &$form_state) {
  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function avrfreaks_ctools_plugins_popular_edit_form_submit($form, &$form_state) {

}

/**
 * Run-time rendering of the body of the block (content type)
 */
function avrfreaks_ctools_plugins_popular_render($subtype, $conf, $panel_args, $context = NULL) {
  $view_to_hide = views_get_view('avr_projects');
  $view_to_hide->set_display('panel_pane_1');
  $view_to_hide->pre_execute();
  $view_to_hide->execute();
  if (empty($view_to_hide->result)) {
    $view_to_display = views_get_view('avr_projects');
    $view_to_display->set_display('panel_pane_5');
    $view_to_display->set_arguments(array(rtrim(arg(0), 's')));
    $view_to_display->pre_execute();
    $view_to_display->execute();

    $block = new stdClass();
    $block->content = $view_to_display->render();
    $block->title = 'Most popular ' . rtrim(arg(0), 's') . ' content';
    return $block;
  }
}