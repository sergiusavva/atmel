Panels Performance

This module adds a new Timer renderer tracks how long it takes each pane to
render and displays the information. This is very helpful for figuring out
which content in a panel is slowing down the page render times.

When you enable the module and it'll modify the standard pipeline to use the new
renderer. Meaning that once you turn this on, anywhere you're using the
standard renderer, everyone will start seeing timing information. So don't turn
this on on a production server!