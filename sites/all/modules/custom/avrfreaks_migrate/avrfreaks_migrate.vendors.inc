<?php

/**
 * Class AvrFreaksUserMigration.
 */
class AvrFreaksVendorsMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Give a description to this process.
    $this->description = t('Migrate Vendors from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_vendor', 'nv');
    $query->leftJoin('nuke_freaks_vendor_country_map', 'vcm', 'nv.id = vcm.vendorId');
    $query->leftJoin('nuke_freaks_country', 'c', 'vcm.countryId = c.id');
    $query->leftJoin('nuke_freaks_vendor_class_map', 'vclm', 'nv.id = vclm.vendorId');
    $query->leftJoin('nuke_freaks_vendor_class', 'cl', 'vclm.classId = cl.id');
    $query->addField('nv', 'id', 'id');
    $query->addField('nv', 'name', 'nv_name');
    $query->addField('nv', 'description', 'nv_description');
    $query->addField('nv', 'status', 'nv_status');
    $query->addField('nv', 'ownerId', 'nv_ownerId');

    $query->addExpression('GROUP_CONCAT(DISTINCT c.name)', 'c_name');
    $query->addExpression('GROUP_CONCAT(DISTINCT c.id)', 'c_id');
    $query->addExpression('GROUP_CONCAT(DISTINCT cl.name)', 'cl_name');
    $query->addExpression('GROUP_CONCAT(DISTINCT cl.id)', 'cl_id');

    $query->groupBy('nv.id');
    $query->orderBy('nv.id', 'ASC');

    $this->source = new MigrateSourceSQL($query);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationNode('vendor');

    // ===================== MAPPING.
    $this->addFieldMapping('status', 'nv_status');
    $this->addFieldMapping('language')->defaultValue('und');
    $this->addFieldMapping('created', 'timestamp');
    $this->addFieldMapping('title_field', 'nv_name');
    $this->addFieldMapping('title', 'nv_name');
    $this->addFieldMapping('body', 'nv_description');
    $this->addFieldMapping('body:format')
      ->defaultValue('full_html');

    $this->addFieldMapping('uid', 'nv_ownerId')
      ->sourceMigration('phpbb_AvrFreaks_User');

    $this->addFieldMapping('field_country', 'c_id')
      ->separator(',')
      ->sourceMigration('AvrFreaksCountries')
      ->arguments(array('source_type' => 'tid'));

    $this->addFieldMapping('field_class', 'cl_id')
      ->separator(',')
      ->sourceMigration('AvrFreaksVendorsClasses')
      ->arguments(array('source_type' => 'tid'));

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'phpNuke Unique ID',
          'alias' => 'nv',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->removeFieldMapping('pathauto');
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Prepare timestamp field.
    if (!empty($row->updated) && $row->updated != '0000-00-00 00:00:00') {
      $row->timestamp = strtotime($row->updated);
    }
    else {
      $row->timestamp = '631152000'; // 01-01-1990 00:00:00 (m.d.Y HIS)
    }

    return TRUE;
  }
}

/**
 * Class AvrFreaksCountriesMigration.
 */
class AvrFreaksCountriesMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate Countries from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_country', 'nc')
      ->fields('nc', array('id', 'name', 'region'));
    $this->source = new MigrateSourceSQL($query);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationTerm('countries');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'nc',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // ===================== MAPPING.
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('field_region', 'region');
  }
}

/**
 * Class AvrFreaksVendorsClasses.
 */
class AvrFreaksVendorsClassesMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate Vendor Classes from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_vendor_class', 'vc')
      ->fields('vc', array('id', 'name', 'sort'));
    $this->source = new MigrateSourceSQL($query);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationTerm('vendors_classes');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'vc',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // ===================== MAPPING.
    $this->addFieldMapping('name', 'name');
    $this->addFieldMapping('sort', 'weight');
  }
}
