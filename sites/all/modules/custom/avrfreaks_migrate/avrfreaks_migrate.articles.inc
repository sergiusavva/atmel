<?php

/**
 * Class AvrFreaksUserMigration.
 */
class AvrFreaksArticlesMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Give a description to this process.
    $this->description = t('Migrate Articles from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_article', 'na')
      ->fields('na');
    $query->orderBy('na.id', 'ASC');

    $this->source = new MigrateSourceSQL($query);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationNode('article');

    // ===================== MAPPING.
    $public_stream_wrapper = file_stream_wrapper_get_instance_by_scheme('public');
    $files_folder = DRUPAL_ROOT . '/' . $public_stream_wrapper->getDirectoryPath() . '/';
    $path = $files_folder . 'PNphpArticles';

    $this->addFieldMapping('status', 'approved');
    $this->addFieldMapping('language')->defaultValue('und');
    $this->addFieldMapping('created', 'ts');
    $this->addFieldMapping('title', 'name');
    $this->addFieldMapping('body', 'summary');
    $this->addFieldMapping('body:format')
      ->defaultValue('full_html');
    $this->addFieldMapping('field_written_by', 'writtenby');
    $this->addFieldMapping('field_attach', 'file');
    $this->addFieldMapping('field_attach:source_dir')
      ->defaultValue($path);
    $this->addFieldMapping('field_attach:file_replace')
      ->defaultValue(MigrateFile::FILE_EXISTS_REUSE);

    $this->addFieldMapping('uid', 'uid')
      ->sourceMigration('phpbb_AvrFreaks_User');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'phpNuke Unique ID',
          'alias' => 'na',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    $this->removeFieldMapping('pathauto');
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Prepare timestamp field.
    if (!empty($row->timestamp) && $row->timestamp != '0000-00-00 00:00:00') {
      $row->ts = strtotime($row->timestamp);
    }
    else {
      $row->ts = '1136073600'; // 01-01-2006 00:00:00 (m.d.Y HIS)
    }

    return TRUE;
  }
}
