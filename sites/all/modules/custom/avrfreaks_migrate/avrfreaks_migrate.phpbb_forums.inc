<?php

/**
 * Class AvrFreaksPHPbbForumsMigration.
 */
class AvrFreaksPHPbbForumsMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Give a description to this process.
    $this->description = t('Migrate Forums From CSV');

    // ===================== SOURCE.
    $columns = array(
      0 => array('forum_id', 'Id'),
      1 => array('forum_title', 'Name'),
      2 => array('parent_name', 'Category'),
      3 => array('forum_order', 'Weight'),
    );

    $this->source = new MigrateSourceCSV(drupal_get_path('module', 'avrfreaks_migrate') . '/sources/phpbb_forums.csv', $columns, array(), $this->fields());

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationTerm('forums');

    // ===================== MAPPING.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'forum_id' => array('type' => 'int',
          'not null' => TRUE,
          'description' => 'Forum ID',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    $this->addFieldMapping('name', 'forum_title')
      ->callbacks('html_entity_decode');

    $this->addFieldMapping('parent_name', 'parent_name');
    $this->addFieldMapping('weight', 'forum_order');
    $this->addFieldMapping('format')->defaultValue('plain_text');
    $this->addFieldMapping('language')->defaultValue(LANGUAGE_NONE);
    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'description',
      'path',
    ));
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    return parent::prepareRow($row);
  }

  public function fields() {
    return array(
      'forum_id' => 'Category id.',
      'forum_title' => 'Category title.',
      'forum_order' => 'Category order.',
      'parent_name' => 'Parent name.',
    );
  }
}