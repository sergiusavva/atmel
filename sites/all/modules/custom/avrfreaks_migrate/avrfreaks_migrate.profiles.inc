<?php

/**
 * Class AvrFreaksUserMigration.
 */
class AvrFreaksProfilesMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate Users Profiles from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_users', 'nu');
    $query->fields('nu');
    $query->innerJoin('nuke_phpbb_users', 'bb', 'bb.user_id = nu.pn_uid');

    // ===================== DESTINATION.
    $this->source = new MigrateSourceSQL($query);
    $this->destination = new MigrateDestinationUser();
    $this->systemOfRecord = Migration::DESTINATION;

    // ===================== MAPPING.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'pn_uid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Account ID.',
          'alias' => 'nu',
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );

    $this->addFieldMapping('uid', 'pn_uid')
      ->sourceMigration('phpbb_AvrFreaks_User');

    // User Profile.
    $this->addFieldMapping('field_user_location', 'pn_user_from');
    $this->addFieldMapping('field_offsite_url', 'pn_url');
    $this->addFieldMapping('field_longitude', 'pn_longitude');
    $this->addFieldMapping('field_latitude', 'pn_latitude');
    $this->addFieldMapping('field_occupation', 'pn_user_occ');
    $this->addFieldMapping('field_bio', 'pn_user_intrest');
    $this->addFieldMapping('field_bio:format')
      ->defaultValue('full_html');
    $this->addFieldMapping('field_yahoo_messenger_id', 'pn_user_yim');
    $this->addFieldMapping('field_msn_messenger_id', 'pn_user_msnm');
    $this->addFieldMapping('field_icq_number', 'pn_user_icq');
    $this->addFieldMapping('field_america_online_address', 'pn_user_aim');

    // ===================== ANOTHER.
    $this->removeFieldMapping('pathauto');
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    return TRUE;
  }
}
