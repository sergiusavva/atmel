<?php


class CustomEntityUpdate {

  static public $element_count = 0;
  public $entity_type;
  public $entity_keys;
  public $entity_bundles = array();
  public $widget_types = array();
  public $bundle_key;
  public $entity_field_instance = array();

  function __construct($type = 'node') {
    $this->entity_type = $type;
    $this->init();
  }

  function init() {
    $this->setBundles();
  }

  /**
   * Get bundles from entity info.
   */
  public function setBundles() {

    // Get entity info.
    $entity_info = entity_get_info($this->entity_type);

    // Set entity keys.
    $this->entity_keys = $entity_info['entity keys'];

    // Set bundle keys.
    $this->bundle_key = $entity_info['bundle keys']['bundle'];

    // Set bundles.
    $this->entity_bundles = $entity_info['bundles'];

    foreach ($this->entity_bundles as $key => $bundle) {
      try {
        $this->entity_field_instance[$key] = $this->getBundleFields($key);
      } catch (Exception $e) {
        dpm($e->getMessage(), "Error $bundle");
      }
    }
  }

  /**
   * Get fields from instance.
   * @param $bundle
   * @return array
   */
  public function getBundleFields($bundle) {
    $fields = array();
    foreach (field_info_instances($this->entity_type, $bundle) as $instance) {
      if (in_array($instance['widget']['type'], $this->widget_types)) {
        $fields [$instance['field_name']] = $instance;
      }
    }
    return $fields;
  }

  public function loadEntities() {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', $this->entity_type)
      ->range(0, 1)
      ->execute();

    return empty($result[$this->entity_type]) ? array() : $result[$this->entity_type];
  }

  function processEntity($item) {
    // id set dynamically
    // Load entity.
    $entity_id = $this->entity_keys['id'];
    $entity = entity_metadata_wrapper($this->entity_type, $item->$entity_id);

    // Get fields from object.
    $flag = FALSE;
    // Check all fields
    foreach ($this->entity_field_instance[$item->type] as $field_name => $field) {
      $flag = $this->processField($entity, $field, $field_name);
    }

    // If entity was changed.
    if ($flag) {
      $entity->save();
//      watchdog('AVRFreaks Migrate', t('Entity !id saved.', array('!id' => $entity->getIdentifier())));
    }

    // Destroy object.
    unset($entity);
  }

  function processField(&$entity, $field, $field_name) {
    $values = $entity->{$field_name}->value();
    if ($this->processString($values['value'])) {
      watchdog('Migrate', t('Entity !id. Changed field : !field',
        array(
          '!id' => $entity->$entity_id->value(),
          '!field' => $field_name
        )));
      $entity->{$field_name}->value = $values['value'];
      return TRUE;
    }
    return FALSE;
  }

  function processString(&$value) {
    $string = $value;
    $string = preg_replace('/(\<img[^\>]*src=")(\/preview\!www\.tpl\.org)([^"]*")/', "$1/sites/default/files$3", $string);
    $string = preg_replace('/(\<a[^\>]*href=")([^"]*){0,1}(\/preview\!www\.tpl\.org)([^"]*")/', "$1$4", $string);
    $string = preg_replace('/(\<a[^\>]*href=")([^"]*){0,1}(cloud\.tpl\.org)([^"]*")/', "$1/sites/default/files/cloud.tpl.org$4", $string);
    if (md5($string) != md5($value)) {
      $value = $string;
      return TRUE;
    }
    return FALSE;
  }

}