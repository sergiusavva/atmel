<?php

class AvrfreaksRemapMigration extends MigrationBase {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t("Migrate script to move data from keywords to initiative.");
  }

  // Implement any action you want to occur during an import process in an
  // import() method (alternatively, if you have an action which you want to
  // run during rollbacks, define a rollback() method).
  public function import() {
    $obj = new AvrfreaksRemap();
    foreach ($obj->loadEntities() as $item) {
      $obj->processEntity($item);
    }
    // import() must return one of the MigrationBase RESULT constants.
    return MigrationBase::RESULT_COMPLETED;
  }

  /**
   * Define isComplete(), returning a boolean, to indicate whether dependent migrations may proceed.
   */
  public function isComplete() {
    return TRUE;
  }
}

class AvrfreaksDeviceRemapMigration extends MigrationBase {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t("Migrate script to move data from keywords to initiative.");
  }

  // Implement any action you want to occur during an import process in an
  // import() method (alternatively, if you have an action which you want to
  // run during rollbacks, define a rollback() method).
  public function import() {
    $obj = new AvrfreaksDeviceRemap();
    foreach ($obj->loadEntities() as $item) {
      $obj->processEntity($item);
    }
    // import() must return one of the MigrationBase RESULT constants.
    return MigrationBase::RESULT_COMPLETED;
  }

  /**
   * Define isComplete(), returning a boolean, to indicate whether dependent migrations may proceed.
   */
  public function isComplete() {
    return TRUE;
  }
}

/**
 * Class AvrfreaksRemap
 * Delete old terms from nodes.
 */
class AvrfreaksRemap extends CustomEntityUpdate {

  public function loadEntities() {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', $this->entity_type)
      ->entityCondition('bundle', array('forum'), 'IN')
//      ->propertyCondition('nid', 108617, '>')
//      ->range(0, 110)
      ->execute();

    return empty($result[$this->entity_type]) ? array() : $result[$this->entity_type];
  }

  /**
   * Get fields from instance.
   * @param $bundle
   * @return array
   */
  public function getBundleFields($bundle) {
    $fields_name = array('taxonomy_forums');
    $fields = array();
    foreach (field_info_instances($this->entity_type, $bundle) as $instance) {
      if (in_array($instance['field_name'], $fields_name)) {
        $fields [$instance['field_name']] = $instance;
      }
    }
    return $fields;
  }

  /**
   * Process field callback.
   * @param EntityMetadataWrapper $entity
   * @param $field
   * @param $field_name
   * @return bool
   */
  function processField(&$entity, $field, $field_name) {
    $changes = FALSE;
    if ('taxonomy_forums' == $field_name) {
      $term_id = $entity->$field_name->getIdentifier();
      $new_value = $this->mapping($term_id);
      if ($new_value !== FALSE) {
        $entity->{$field_name}->set($new_value);
        $changes = TRUE;
      }
    }
    return $changes;
  }

  /**
   * Give new term_id.
   * @param $keyword
   * @return bool
   */
  function mapping($keyword) {
    $map = array(
      //keyword => initiative
      '204' => '581', // AVR32 Linux Forum
      '209' => '581', // AVR32 General (standalone)
      '208' => '586', // XMEGA forum
      '211' => '596', // AVR Wireless forum.
      '199' => '591', // AVR forum
      '215' => '606', // AVR32 Hardware
//      '213' => '611', // AVR32 Software Tools
      '217' => '621', // AVR studio 4 forum
      '213' => '621', // AVR32 Software Tools
      '216' => '621', // AVR Studio 5 and Atmel Studio 6 forum
      '194' => '626', // AVR Software Framework
      '203' => '626', // AVR Software Framework
      '212' => '631', // AVR GCC forum
      '219' => '636', // AVR Tutorials
      '202' => '641', // General Electronics
      '201' => '646', // Off-topic forum
      '205' => '651', // AVRfreaks Trading Post
      '200' => '656', // AVRfreaks.net Housekeeping
      '214' => '510', // AVRfreaks Mods
    );
    return empty($map[$keyword]) ? FALSE : $map[$keyword];
  }
}


/**
 * Class AvrfreaksRemap
 * Delete old terms from nodes.
 */
class AvrfreaksDeviceRemap extends CustomEntityUpdate {

  public function loadEntities() {
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', $this->entity_type)
      ->entityCondition('bundle', array('forum'), 'IN')
      ->propertyCondition('nid', 130946, '>')
//      ->range(0, 100)
      ->execute();

    return empty($result[$this->entity_type]) ? array() : $result[$this->entity_type];
  }

  /**
   * Get fields from instance.
   * @param $bundle
   * @return array
   */
  public function getBundleFields($bundle) {
    $fields_name = array('field_avr_forum_devices');
    $fields = array();
    foreach (field_info_instances($this->entity_type, $bundle) as $instance) {
      if (in_array($instance['field_name'], $fields_name)) {
        $fields [$instance['field_name']] = $instance;
      }
    }
    return $fields;
  }

  /**
   * Process field callback.
   * @param EntityMetadataWrapper $entity
   * @param $field
   * @param $field_name
   * @return bool
   */
  function processField(&$entity, $field, $field_name) {
    $changes = FALSE;
    if ('field_avr_forum_devices' == $field_name) {
      try {
        if (isset($entity->body)) {
          $body = $entity->body->value->value();
          $values = $this->check_text($body);

          if (!empty($values)) {
            $entity->$field_name->set($values);
            $changes = TRUE;
          }
        }
      } catch (Exception $e) {
        watchdog('Migrate', $entity->getIdentifier());
      }
    }
    return $changes;
  }

  function check_text($text = '') {
    $arr = array();
    static $keys;

    if (empty($keys)) {
      $keys = $this->key_words();
    }

    foreach ($keys as $item) {
      if ((bool) preg_match("^($item[0])|($item[1])^", $text)) {
        $arr[] = $item[2];
      }
    }
    return $arr;
  }

  function key_words() {
    static $arr;
    if (empty($arr)) {
      $arr = array(
        array("ATxmega128A1", "xmega128A1", 662),
        array("ATxmega128A1U", "xmega128A1U", 663),
        array("ATxmega128A3", "xmega128A3", 664),
        array("ATxmega128A3U", "xmega128A3U", 665),
        array("ATxmega128A4U", "xmega128A4U", 666),
        array("ATxmega16A4", "xmega16A4", 667),
        array("ATxmega16A4U", "xmega16A4U", 668),
        array("ATxmega192A3", "xmega192A3", 669),
        array("ATxmega192A3U", "xmega192A3U", 670),
        array("ATxmega256A3", "xmega256A3", 671),
        array("ATxmega256A3B", "xmega256A3B", 672),
        array("ATxmega256A3BU", "xmega256A3BU", 673),
        array("ATxmega256A3U", "xmega256A3U", 674),
        array("ATxmega32A4", "xmega32A4", 675),
        array("ATxmega32A4U", "xmega32A4U", 676),
        array("ATxmega64A1", "xmega64A1", 677),
        array("ATxmega64A1U", "xmega64A1U", 678),
        array("ATxmega64A3", "xmega64A3", 679),
        array("ATxmega64A3U", "xmega64A3U", 680),
        array("ATxmega64A4U", "xmega64A4U", 681),
        array("AT32UC3A0128", "32UC3A0128", 682),
        array("AT32UC3A0256", "32UC3A0256", 683),
        array("AT32UC3A0512", "32UC3A0512", 684),
        array("AT32UC3A1128", "32UC3A1128", 685),
        array("AT32UC3A1256", "32UC3A1256", 686),
        array("AT32UC3A1512", "32UC3A1512", 687),
        array("AT32UC3A0128AU", "32UC3A0128AU", 688),
        array("AT32UC3A0256AU", "32UC3A0256AU", 689),
        array("AT32UC3A0512AU", "32UC3A0512AU", 690),
        array("AT32UC3A1256AU", "32UC3A1256AU", 691),
        array("AT32UC3A1512AU", "32UC3A1512AU", 692),
        array("AT32UC3A3128", "32UC3A3128", 693),
        array("AT32UC3A3128S", "32UC3A3128S", 694),
        array("AT32UC3A3256", "32UC3A3256", 695),
        array("AT32UC3A3256S", "32UC3A3256S", 696),
        array("AT32UC3A364", "32UC3A364", 697),
        array("AT32UC3A364S", "32UC3A364S", 698),
        array("AT32UC3A4128", "32UC3A4128", 699),
        array("AT32UC3A4128S", "32UC3A4128S", 700),
        array("AT32UC3A4256", "32UC3A4256", 701),
        array("AT32UC3A4256S", "32UC3A4256S", 702),
        array("AT32UC3A464", "32UC3A464", 703),
        array("AT32UC3A464S", "32UC3A464S", 704),
        array("AT32UC3A3256AU", "32UC3A3256AU", 705),
        array("AT90CAN128 Automotive", "90CAN128 Automotive", 706),
        array("AT90CAN32 Automotive", "90CAN32 Automotive", 707),
        array("AT90CAN64 Automotive", "90CAN64 Automotive", 708),
        array("ATmega16M1 Automotive", "mega16M1 Automotive", 709),
        array("ATmega32C1 Automotive", "mega32C1 Automotive", 710),
        array("ATmega32M1 Automotive", "mega32M1 Automotive", 711),
        array("ATmega64C1 Automotive", "mega64C1 Automotive", 712),
        array("ATmega64M1 Automotive", "mega64M1 Automotive", 713),
        array("AT32UC3C0512C Automotive", "32UC3C0512C Automotive", 714),
        array("AT32UC3C1512C Automotive", "32UC3C1512C Automotive", 715),
        array("AT32UC3C2512C Automotive", "32UC3C2512C Automotive", 716),
        array("ATmega164P Automotive", "mega164P Automotive", 717),
        array("ATmega164P-B Automotive", "mega164P-B Automotive", 718),
        array("ATmega168 Automotive", "mega168 Automotive", 719),
        array("ATmega168PA Automotive", "mega168PA Automotive", 720),
        array("ATmega169P Automotive", "mega169P Automotive", 721),
        array("ATmega324P Automotive", "mega324P Automotive", 722),
        array("ATmega324P-B Automotive ", "mega324P-B Automotive ", 723),
        array("ATmega328P Automotive", "mega328P Automotive", 724),
        array("ATmega48 Automotive", "mega48 Automotive", 725),
        array("ATmega48PA Automotive", "mega48PA Automotive", 726),
        array("ATmega644P Automotive", "mega644P Automotive", 727),
        array("ATmega644P-B Automotive", "mega644P-B Automotive", 728),
        array("ATmega88 Automotive", "mega88 Automotive", 729),
        array("ATmega88PA Automotive", "mega88PA Automotive", 730),
        array("ATtiny167 Automotive", "tiny167 Automotive", 731),
        array("ATtiny24 Automotive", "tiny24 Automotive", 732),
        array("ATtiny25 Automotive", "tiny25 Automotive", 733),
        array("ATtiny261 Automotive", "tiny261 Automotive", 734),
        array("ATtiny44 Automotive", "tiny44 Automotive", 735),
        array("ATtiny45 Automotive", "tiny45 Automotive", 736),
        array("ATtiny461 Automotive", "tiny461 Automotive", 737),
        array("ATtiny84 Automotive", "tiny84 Automotive", 738),
        array("ATtiny85 Automotive", "tiny85 Automotive", 739),
        array("ATtiny861 Automotive", "tiny861 Automotive", 740),
        array("ATtiny87 Automotive", "tiny87 Automotive", 741),
        array("ATtiny88 Automotive", "tiny88 Automotive", 742),
        array("ATxmega128B1", "xmega128B1", 743),
        array("ATxmega128B3", "xmega128B3", 744),
        array("ATxmega64B1", "xmega64B1", 745),
        array("ATxmega64B3", "xmega64B3", 746),
        array("AT32UC3B0128", "32UC3B0128", 747),
        array("AT32UC3B0256", "32UC3B0256", 748),
        array("AT32UC3B0512", "32UC3B0512", 749),
        array("AT32UC3B064", "32UC3B064", 750),
        array("AT32UC3B1128", "32UC3B1128", 751),
        array("AT32UC3B1256", "32UC3B1256", 752),
        array("AT32UC3B1512", "32UC3B1512", 753),
        array("AT32UC3B164", "32UC3B164", 754),
        array("AT32UC3B0128AU", "32UC3B0128AU", 755),
        array("AT32UC3B0512AU", "32UC3B0512AU", 756),
        array("ATmega16HVA", "mega16HVA", 757),
        array("ATmega16HVB", "mega16HVB", 758),
        array("ATmega32HVB", "mega32HVB", 759),
        array("ATmega406", "mega406", 760),
        array("ATmega8HVA", "mega8HVA", 761),
        array("ATxmega128C3", "xmega128C3", 762),
        array("ATxmega16C4", "xmega16C4", 763),
        array("ATxmega192C3", "xmega192C3", 764),
        array("ATxmega256C3", "xmega256C3", 765),
        array("ATxmega32C3", "xmega32C3", 766),
        array("ATxmega32C4", "xmega32C4", 767),
        array("ATxmega384C3", "xmega384C3", 768),
        array("ATxmega64C3", "xmega64C3", 769),
        array("AT32UC3C0128C", "32UC3C0128C", 770),
        array("AT32UC3C0256C", "32UC3C0256C", 771),
        array("AT32UC3C0512C", "32UC3C0512C", 772),
        array("AT32UC3C064C", "32UC3C064C", 773),
        array("AT32UC3C1128C", "32UC3C1128C", 774),
        array("AT32UC3C1256C", "32UC3C1256C", 775),
        array("AT32UC3C1512C", "32UC3C1512C", 776),
        array("AT32UC3C164C", "32UC3C164C", 777),
        array("AT32UC3C2128C", "32UC3C2128C", 778),
        array("AT32UC3C2256C", "32UC3C2256C", 779),
        array("AT32UC3C2512C", "32UC3C2512C", 780),
        array("AT32UC3C264C", "32UC3C264C", 781),
        array("AT32UC3C0512CAU", "32UC3C0512CAU", 782),
        array("ATxmega128D3", "xmega128D3", 783),
        array("ATxmega128D4", "xmega128D4", 784),
        array("ATxmega16D4", "xmega16D4", 785),
        array("ATxmega192D3", "xmega192D3", 786),
        array("ATxmega256D3", "xmega256D3", 787),
        array("ATxmega32D3", "xmega32D3", 788),
        array("ATxmega32D4", "xmega32D4", 789),
        array("ATxmega384D3", "xmega384D3", 790),
        array("ATxmega64D3", "xmega64D3", 791),
        array("ATxmega64D4", "xmega64D4", 792),
        array("ATUC128D3", "UC128D3", 793),
        array("ATUC128D4", "UC128D4", 794),
        array("ATUC64D3", "UC64D3", 795),
        array("ATUC64D4", "UC64D4", 796),
        array("ATxmega16E5", "xmega16E5", 797),
        array("ATxmega32E5", "xmega32E5", 798),
        array("ATxmega8E5", "xmega8E5", 799),
        array("AT32UC3L0128", "32UC3L0128", 800),
        array("AT32UC3L016", "32UC3L016", 801),
        array("AT32UC3L0256", "32UC3L0256", 802),
        array("AT32UC3L032", "32UC3L032", 803),
        array("AT32UC3L064", "32UC3L064", 804),
        array("ATUC128L3U", "UC128L3U", 805),
        array("ATUC128L4U", "UC128L4U", 806),
        array("ATUC256L3U", "UC256L3U", 807),
        array("ATUC256L4U", "UC256L4U", 808),
        array("ATUC64L3U", "UC64L3U", 809),
        array("ATUC64L4U", "UC64L4U", 810),
        array("AT32AP7000", "32AP7000", 811),
        array("AT32AP7001", "32AP7001", 812),
        array("AT32AP7002", "32AP7002", 813),
        array("AT90S1200", "90S1200", 814),
        array("AT90S2313", "90S2313", 815),
        array("AT90S2323", "90S2323", 816),
        array("AT90S2343", "90S2343", 817),
        array("AT90S4433", "90S4433", 818),
        array("AT90S8515", "90S8515", 819),
        array("AT90S8535", "90S8535", 820),
        array("AT90CAN128", "90CAN128", 821),
        array("AT90CAN32", "90CAN32", 822),
        array("AT90CAN64", "90CAN64", 823),
        array("AT90PWM1", "90PWM1", 824),
        array("AT90PWM161", "90PWM161", 825),
        array("AT90PWM2", "90PWM2", 826),
        array("AT90PWM216", "90PWM216", 827),
        array("AT90PWM2B", "90PWM2B", 828),
        array("AT90PWM3", "90PWM3", 829),
        array("AT90PWM316", "90PWM316", 830),
        array("AT90PWM3B", "90PWM3B", 831),
        array("AT90PWM81", "90PWM81", 832),
        array("AT90USB1286", "90USB1286", 833),
        array("AT90USB1287", "90USB1287", 834),
        array("AT90USB162", "90USB162", 835),
        array("AT90USB646", "90USB646", 836),
        array("AT90USB647", "90USB647", 837),
        array("AT90USB82", "90USB82", 838),
        array("ATmega103", "mega103", 839),
        array("ATmega128", "mega128", 840),
        array("ATmega1280", "mega1280", 841),
        array("ATmega1281", "mega1281", 842),
        array("ATmega1284", "mega1284", 843),
        array("ATmega1284P", "mega1284P", 844),
        array("ATmega128A", "mega128A", 845),
        array("ATmega16", "mega16", 846),
        array("ATmega161", "mega161", 847),
        array("ATMEGA162", "MEGA162", 848),
        array("ATmega163", "mega163", 849),
        array("ATmega164A", "mega164A", 850),
        array("ATmega164P", "mega164P", 851),
        array("ATmega164PA", "mega164PA", 852),
        array("ATmega165", "mega165", 853),
        array("ATmega165P", "mega165P", 854),
        array("ATmega165PA", "mega165PA", 855),
        array("ATmega168", "mega168", 856),
        array("ATmega168A", "mega168A", 857),
        array("ATmega168P", "mega168P", 858),
        array("ATmega168PA", "mega168PA", 859),
        array("ATmega169", "mega169", 860),
        array("ATmega169A", "mega169A", 861),
        array("ATmega169P", "mega169P", 862),
        array("ATmega169PA", "mega169PA", 863),
        array("ATmega16A", "mega16A", 864),
        array("ATmega16M1", "mega16M1", 865),
        array("ATmega16U2", "mega16U2", 866),
        array("ATmega16U4", "mega16U4", 867),
        array("ATmega2560", "mega2560", 868),
        array("ATmega2561", "mega2561", 869),
        array("ATmega32", "mega32", 870),
        array("ATmega323", "mega323", 871),
        array("ATmega324A", "mega324A", 872),
        array("ATmega324P", "mega324P", 873),
        array("ATmega324PA", "mega324PA", 874),
        array("ATmega325", "mega325", 875),
        array("ATmega3250", "mega3250", 876),
        array("ATmega3250A", "mega3250A", 877),
        array("ATmega3250P", "mega3250P", 878),
        array("ATmega325A", "mega325A", 879),
        array("ATmega325P", "mega325P", 880),
        array("ATmega325PA", "mega325PA", 881),
        array("ATmega328", "mega328", 882),
        array("ATmega328P", "mega328P", 883),
        array("ATmega329", "mega329", 884),
        array("ATmega3290", "mega3290", 885),
        array("ATmega3290A", "mega3290A", 886),
        array("ATmega3290P", "mega3290P", 887),
        array("ATmega329A", "mega329A", 888),
        array("ATmega329P", "mega329P", 889),
        array("ATmega329PA", "mega329PA", 890),
        array("ATmega32A", "mega32A", 891),
        array("ATmega32M1", "mega32M1", 892),
        array("ATmega32U2", "mega32U2", 893),
        array("ATmega32U4", "mega32U4", 894),
        array("ATmega48", "mega48", 895),
        array("ATmega48A", "mega48A", 896),
        array("ATmega48P", "mega48P", 897),
        array("ATmega48PA", "mega48PA", 898),
        array("ATmega64", "mega64", 899),
        array("ATmega640", "mega640", 900),
        array("ATmega644", "mega644", 901),
        array("ATmega644A", "mega644A", 902),
        array("ATmega644P", "mega644P", 903),
        array("ATmega644PA", "mega644PA", 904),
        array("ATmega645", "mega645", 905),
        array("ATmega6450", "mega6450", 906),
        array("ATmega6450A", "mega6450A", 907),
        array("ATmega6450P", "mega6450P", 908),
        array("ATmega645A", "mega645A", 909),
        array("ATmega645P", "mega645P", 910),
        array("ATmega649", "mega649", 911),
        array("ATmega6490", "mega6490", 912),
        array("ATmega6490A", "mega6490A", 913),
        array("ATmega6490P", "mega6490P", 914),
        array("ATmega649A", "mega649A", 915),
        array("ATmega649P", "mega649P", 916),
        array("ATmega64A", "mega64A", 917),
        array("ATmega64M1", "mega64M1", 918),
        array("ATmega8", "mega8", 919),
        array("ATmega8515", "mega8515", 920),
        array("ATmega8535", "mega8535", 921),
        array("ATmega88", "mega88", 922),
        array("ATmega88A", "mega88A", 923),
        array("ATmega88P", "mega88P", 924),
        array("ATmega88PA", "mega88PA", 925),
        array("ATmega8A", "mega8A", 926),
        array("ATmega8U2", "mega8U2", 927),
        array("ATtiny10", "tiny10", 928),
        array("ATtiny11", "tiny11", 929),
        array("ATtiny12", "tiny12", 930),
        array("ATtiny13", "tiny13", 931),
        array("ATtiny13A", "tiny13A", 932),
        array("ATtiny15L", "tiny15L", 933),
        array("ATtiny1634", "tiny1634", 934),
        array("ATtiny167", "tiny167", 935),
        array("ATtiny20", "tiny20", 936),
        array("ATtiny2313", "tiny2313", 937),
        array("ATtiny2313A", "tiny2313A", 938),
        array("ATtiny24", "tiny24", 939),
        array("ATtiny24A", "tiny24A", 940),
        array("ATtiny25", "tiny25", 941),
        array("ATtiny26", "tiny26", 942),
        array("ATtiny261", "tiny261", 943),
        array("ATtiny261A", "tiny261A", 944),
        array("ATtiny28L", "tiny28L", 945),
        array("ATtiny4", "tiny4", 946),
        array("ATtiny40", "tiny40", 947),
        array("ATtiny4313", "tiny4313", 948),
        array("ATtiny43U", "tiny43U", 949),
        array("ATtiny44", "tiny44", 950),
        array("ATtiny441", "tiny441", 951),
        array("ATtiny44A", "tiny44A", 952),
        array("ATtiny45", "tiny45", 953),
        array("ATtiny461", "tiny461", 954),
        array("ATtiny461A", "tiny461A", 955),
        array("ATtiny48", "tiny48", 956),
        array("ATtiny5", "tiny5", 957),
        array("ATtiny828", "tiny828", 958),
        array("ATtiny84", "tiny84", 959),
        array("ATtiny841", "tiny841", 960),
        array("ATtiny84A", "tiny84A", 961),
        array("ATtiny85", "tiny85", 962),
        array("ATtiny861", "tiny861", 963),
        array("ATtiny861A", "tiny861A", 964),
        array("ATtiny87", "tiny87", 965),
        array("ATtiny88", "tiny88", 966),
        array("ATtiny9", "tiny9", 967)
      );
    }
    return $arr;
  }

}
