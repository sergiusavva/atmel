<?php
/**
 * @file
 * File avrfreaks_migrate.migrate.inc
 */

/**
 * Implements hook_migrate_api().
 */
function avrfreaks_migrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'custom' => array(
        'title' => t('AvrFreaks custom'),
      ),
    ),
    'migrations' => array(
      'AvrFreaksArticles' => array(
        'class_name' => 'AvrFreaksArticlesMigration',
        'group_name' => 'custom',
      ),
      'AvrFreaksCountries' => array(
        'class_name' => 'AvrFreaksCountriesMigration',
        'group_name' => 'custom',
      ),
      'AvrFreaksVendorsClasses' => array(
        'class_name' => 'AvrFreaksVendorsClassesMigration',
        'group_name' => 'custom',
      ),
      'AvrFreaksVendors' => array(
        'class_name' => 'AvrFreaksVendorsMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          'AvrFreaksCountries',
          'AvrFreaksVendorsClasses',
        ),
      ),
      'AvrFreaksProjectsType' => array(
        'class_name' => 'AvrFreaksProjectsTypeMigration',
        'group_name' => 'custom',
      ),
      'AvrFreaksProjectsCompilers' => array(
        'class_name' => 'AvrFreaksProjectsCompilersMigration',
        'group_name' => 'custom',
      ),
      'AvrFreaksProjects' => array(
        'class_name' => 'AvrFreaksProjectsMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          'AvrFreaksProjectsType',
          'AvrFreaksProjectsCompilers',
        ),
      ),
      'AvrFreaksUsers' => array(
        'class_name' => 'AvrFreaksUsersMigration',
        'group_name' => 'custom',
      ),
      'AvrFreaksProfiles' => array(
        'class_name' => 'AvrFreaksProfilesMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          'AvrFreaksUsers',
          'phpbb_AvrFreaks_User',
        ),
      ),
      'AvrFreaksPHPbbCategories' => array(
        'class_name' => 'AvrFreaksPHPbbCategoriesMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          //@TODO : define dependencies
        ),
      ),
      'AvrFreaksPHPbbForums' => array(
        'class_name' => 'AvrFreaksPHPbbForumsMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          //@TODO : define dependencies
        ),
      ),

      'AvrFreaksRemap' => array(
        'class_name' => 'AvrfreaksRemapMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          //@TODO : define dependencies
        ),
      ),
      'AvrFreaksDeviceRemap' => array(
        'class_name' => 'AvrfreaksDeviceRemapMigration',
        'group_name' => 'custom',
        'dependencies' => array(
          //@TODO : define dependencies
        ),
      ),
//      'AvrFreaksPHPbbTopics' => array(
//        'class_name' => 'AvrFreaksPHPbbTopicsMigration',
//        'group_name' => 'custom',
//        'dependencies' => array(
//          //@TODO : define dependencies
//        ),
//      ),
    ),
  );

  return $api;
}
