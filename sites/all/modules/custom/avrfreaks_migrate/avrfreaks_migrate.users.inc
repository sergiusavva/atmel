<?php

/**
 * Class AvrFreaksUserMigration.
 */
class AvrFreaksUsersMigration extends Migration {
  var $defaultLanguage,
    $importFormat = NULL;


  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate Users from phpNuke to Drupal 7');

    // == SOURCE ==.
    $subquery = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_phpbb_users', 'bbu');
    $subquery->fields('bbu', array('user_id'));
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_users', 'nu');
    $query->fields('nu');
    $query->condition('nu.pn_uid', $subquery, 'NOT IN');
    $this->source = new MigrateSourceSQL($query);

    // == DESTINATION ==.
    // Built in destination for users
    $this->destination = new MigrateDestinationUserMerge(MigrateDestinationUser::options(
      $this->defaultLanguage = 'und', $this->importFormat = 'full_html', TRUE
    ));

    // == MAPPING ==.
    $this->addFieldMapping('name', 'pn_uname');
    $this->addFieldMapping('pass', 'pn_pass');
    $this->addFieldMapping('mail', 'pn_email');
    $this->addFieldMapping('signature', 'pn_user_sig');
    $this->addFieldMapping('created', 'pn_user_regdate');
    $this->addFieldMapping('status')->defaultValue(1);

    // User Profile.
    $this->addFieldMapping('field_user_location', 'pn_user_from');
    $this->addFieldMapping('field_offsite_url', 'pn_url');
    $this->addFieldMapping('field_longitude', 'pn_longitude');
    $this->addFieldMapping('field_latitude', 'pn_latitude');
    $this->addFieldMapping('field_occupation', 'pn_user_occ');
    $this->addFieldMapping('field_bio', 'pn_user_intrest');
    $this->addFieldMapping('field_bio:format')
      ->defaultValue('full_html');
    $this->addFieldMapping('field_yahoo_messenger_id', 'pn_user_yim');
    $this->addFieldMapping('field_msn_messenger_id', 'pn_user_msnm');
    $this->addFieldMapping('field_icq_number', 'pn_user_icq');
    $this->addFieldMapping('field_america_online_address', 'pn_user_aim');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'pn_uid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'phpNuke Unique User ID',
          'alias' => 'nu',
        )
      ),
      MigrateDestinationUser::getKeySchema()
    );

    $this->removeFieldMapping('pathauto');

    $this->addUnmigratedDestinations(array(
      'data',
      'is_new',
      'path',
      'role_names',
      'theme',
    ));
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Check for duplicate users.
    $user_by_username = user_load_by_name($row->pn_uid);
    if ($row->pn_email != '') {
      $user_by_email = user_load_by_mail($row->pn_email);
    }
    /**
     * If a user exists with email address then use that username no matter what the source is.
     */
    if ($row->pn_uid == 1) {
      // Skip Anonymous user.
      return FALSE;
    }
    else {
      if ($row->pn_uid == 2 && $this->migration_settings['admin_migrate_option'] == PHPBB2DRUPAL_ADMIN_USER_ADMIN) {
        $admin_user = user_load(1);
        $row->uid = 1;
        $row->name = $admin_user->name;
        $row->pass = $admin_user->pass;
        $row->is_new = FALSE;
        $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
      }
      else {
        if ($user_by_email) {
          $dupe_option = $this->migration_settings['email_collision_option'];
          if ($dupe_option == PHPBB2DRUPAL_DUPE_EMAIL_IGNORE) {
            return FALSE;
          }
          $row->is_new = FALSE;
          $this->rollbackAction = MigrateMap::ROLLBACK_PRESERVE;
        }
        /**
         * If the username is taken by a user with a different email address then generate a user name.
         */
        else {
          if ($user_by_username) {
            if ($this->migration_settings[''] == PHPBB2DRUPAL_DUPE_USERNAME_IGNORE) {
              return FALSE;
            }
          }
        }
      }
    }

    return TRUE;
  }
}
