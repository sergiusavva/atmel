<?php

/**
 * Class AvrFreaksPHPbbCategoriesMigration.
 */
class AvrFreaksPHPbbCategoriesMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Give a description to this process.
    $this->description = t('Migrate Articles from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $columns = array(
      0 => array('cat_id', 'Id'),
      1 => array('cat_title', 'Name'),
      2 => array('cat_order', 'Weight'),
    );

    $this->source = new MigrateSourceCSV(drupal_get_path('module', 'avrfreaks_migrate') . '/sources/phpbb_categories.csv', $columns, array(), $this->fields());

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationTerm('forums');

    // ===================== MAPPING.
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'cat_id' => array('type' => 'int',
          'not null' => TRUE,
          'description' => 'Category ID',
        ),
      ),
      MigrateDestinationTerm::getKeySchema()
    );
    $this->addFieldMapping('name', 'cat_title')
      ->callbacks('html_entity_decode');
    $this->addFieldMapping('weight', 'cat_order');
    $this->addFieldMapping('format')->defaultValue('plain_text');
    $this->addFieldMapping('language')->defaultValue(LANGUAGE_NONE);
    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'description',
      'parent',
      'parent_name',
      'path',
    ));
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    return parent::prepareRow($row);
  }

  public function fields() {
    return array(
      'cat_id' => 'Category id.',
      'cat_title' => 'Category title.',
      'cat_order' => 'Category order.',
    );
  }
}
