<?php

/**
 * Class AvrFreaksUserMigration.
 */
class AvrFreaksProjectsMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    // Give a description to this process.
    $this->description = t('Migrate Projects from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_project', 'fp')
      ->fields('fp');

    $fields = array(
      'timestamp' => t('Created'),
      'timestamp_changed' => t('Changed'),
      'TypeID' => t('Type tid'),
      'title_field' => t('Title field'),
      'attachments' => t('File attachments'),
      'links' => t('Link'),
      'links_titles' => t('Link title'),
    );

    $this->source = new MigrateSourceSQL($query, $fields);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationNode('project');

    // ===================== MAPPING.
    $this->addFieldMapping('status')->defaultValue('1');
    $this->addFieldMapping('language')->defaultValue('und');

    $this->addFieldMapping('created', 'timestamp');
    $this->addFieldMapping('changed', 'timestamp_changed');

    $this->addFieldMapping('title', 'Name');

    $this->addFieldMapping('body', 'LongDesc');
    $this->addFieldMapping('body:summary', 'Short');
    $this->addFieldMapping('body:format')
      ->defaultValue('full_html');
    $this->addFieldMapping('body:language')
      ->defaultValue('und');

    $this->addFieldMapping('field_project_type', 'Type')
      ->sourceMigration('AvrFreaksProjectsType');
    $this->addFieldMapping('field_project_type:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('field_compiler_assembler', 'Compiler')
      ->sourceMigration('AvrFreaksProjectsCompilers');
    $this->addFieldMapping('field_compiler_assembler:source_type')
      ->defaultValue('tid');

    $this->addFieldMapping('field_attach', 'attachments');
    $this->addFieldMapping('field_attach:file_replace')
      ->defaultValue(MigrateFile::FILE_EXISTS_REUSE);

    $this->addFieldMapping('field_links', 'links');
    $this->addFieldMapping('field_links:title', 'links_titles');

    $this->addFieldMapping('uid', 'OwnerID')
      ->sourceMigration('phpbb_AvrFreaks_User');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'ProjectID' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'phpNuke Unique ID',
          'alias' => 'fp',
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );
    $this->removeFieldMapping('pathauto');
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    // Prepare timestamp field.
    if (!empty($row->Added) && $row->Added != '0000-00-00 00:00:00') {
      $row->timestamp = strtotime($row->Added);
      $row->timestamp_changed = strtotime($row->changed);
    }
    else {
      $row->timestamp = '1136073600'; // 01-01-2006 00:00:00 (m.d.Y HIS)
    }

    // attachments
    if ($row->ProjectID) {
      $pid = $row->ProjectID;
      $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_projectfile', 'fp');
      $query->fields('fp', array('project_id', 'file_id'));
      $query->fields('ff', array('type', 'path', 'caption'));
      $query->innerJoin('nuke_freaks_files', 'ff', 'ff.id = fp.file_id');
      $query->condition('fp.project_id', $pid);
      $result = $query->execute();

      while ($entry = $result->fetchAssoc()) {
        if ($entry['type'] == 'FILE') {
          $fids[$pid][$entry['file_id']] = array(
            'fid' => $entry['file_id'],
            'path' => $entry['path'],
            'type' => $entry['type'],
            'caption' => $entry['caption'],
          );
        }
        elseif ($entry['type'] == 'URL') {
          $urls[$pid][$entry['file_id']] = array(
            'fid' => $entry['file_id'],
            'path' => $entry['path'],
            'type' => $entry['type'],
            'caption' => $entry['caption'],
          );
        }
      }

      $public_stream_wrapper = file_stream_wrapper_get_instance_by_scheme('public');
      $files_folder = DRUPAL_ROOT . '/' . $public_stream_wrapper->getDirectoryPath() . '/';
      $path = $files_folder . 'AVRFreaksFiles';

      if (!empty($fids[$pid])) {
        foreach ($fids[$pid] as $key => $value) {
          $file = rtrim($path, '/') . '/' . $value['path'];
          $row->attachments[] = $file;
        }
      }

      if (!empty($urls[$pid])) {
        foreach ($urls[$pid] as $key => $value) {
          $row->links[] = $value['path'];
          $row->links_titles[] = $value['caption'];
        }
      }
    }

    return TRUE;
  }

}

/**
 * Class AvrFreaksVendorsClasses.
 */
class AvrFreaksProjectsTypeMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate Projects Types from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_projecttype', 'pt')
      ->fields('pt', array('TypeID', 'TypeName'));
    $this->source = new MigrateSourceSQL($query);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationTerm('projects_types');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'TypeID' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'pt',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // ===================== MAPPING.
    $this->addFieldMapping('name', 'TypeName');
  }
}

/**
 * Class AvrFreaksVendorsClasses.
 */
class AvrFreaksProjectsCompilersMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Migrate Projects Compilers from phpNuke to Drupal 7');

    // ===================== SOURCE.
    $query = Database::getConnection('default', _avrfreaks_migrate_get_db_name())->select('nuke_freaks_project', 'pt');
    $query->fields('pt', array('ProjectID', 'Compiler'));
    $query->fields('ft', array('id', 'name'));
    $query->innerJoin('nuke_freaks_tools', 'ft', 'ft.id = pt.Compiler');
    $query->groupBy('pt.Compiler');
    $this->source = new MigrateSourceSQL($query);

    // ===================== DESTINATION.
    $this->destination = new MigrateDestinationTerm('compilers');

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'ft',
        )
      ),
      MigrateDestinationTerm::getKeySchema()
    );

    // ===================== MAPPING.
    $this->addFieldMapping('name', 'compiller_name');
  }

  /**
   * Prepare Row.
   */
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    if (!is_null($row->name)) {
      $row->compiller_name = $row->name;
    }
    return TRUE;
  }
}
