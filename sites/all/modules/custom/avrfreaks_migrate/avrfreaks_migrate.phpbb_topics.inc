<?php

/**
 * Class AvrFreaksPHPbbTopicsMigration.
 */
class AvrFreaksPHPbbTopicsMigration extends Migration {
  static $tags_preg_expr;
  static $tags_matches = array();

  static $phpbb_type = 'Topic';

  /**
   * The major version of phpBB (2 and 3 are supported).
   *
   * @var int
   */
  protected $phpbbVersion = 2;

  static $forum_map = array(
    '12'  => 'AVR UC3',
    '13'  => 'AVR UC3',
    '22'  => 'AVR XMEGA',
    '3'   => 'megaAVR and tinyAVR',
    '24'  => 'Wireless',
    '15'  => 'Evaluation and Development Kits',
    '14'  => 'In-System Debuggers and Programmers',
    '7'   => 'Atmel Studio',
//    '14'  => 'Atmel Studio',
    '23'  => 'Atmel Studio',
    '21'  => 'Atmel Software Framework (ASF)',
    '2'   => 'Compilers',
    '11'  => 'AVR Tutorials',
    '16'  => 'General Electronics',
    '9'   => 'Off Topic',
    '8'   => 'Marketplace',
    '4'   => 'Housekeeping',
    '19'  => 'Administratively Speaking',
  );

  static $forum_keywords_map = array(
    'megaAVR' => 'megaAVR and tinyAVR',
    'tinyAVR' => 'megaAVR and tinyAVR',
    'XMEGA'   => 'AVR XMEGA',
    'AVR32'   => 'AVR UC3',
    'Crypto'  => 'General AVR Discussions',
  );

  public function __construct($arguments) {

    Database::addConnectionInfo('avrfreaks_phpbb_database_connection', 'default', array(
      'driver' => 'mysql',
      'database' => 'atmel_orig2',
      'username' => 'root',
      'password' => 'propeople',
      'host' => 'localhost',
      'prefix' => 'nuke_phpbb_',
    ));

    parent::__construct($arguments);
    $this->description = t('Migrate individual forum topics from phpbb.');
//    $this->dependencies = array($this->getMigrationName('User'), $this->getMigrationName('Forums'));
    $this->source = new MigrateSourceSQL($this->query(), array(), NULL,
      array('map_joinable' => FALSE));

    $this->destination = new MigrateDestinationNode('forum',
      array('text_format' => 'full_html')
    );

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'post_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'p'
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    // Put NULL in here due to error http://drupal.org/node/839770
    $this->addFieldMapping('nid')->defaultValue(NULL);



//    $this->addFieldMapping('uid')->defaultValue(1);
    $this->addFieldMapping('uid', 'poster_id')
      ->sourceMigration('phpbb_AvrFreaks_User')->defaultValue(1);              //!!!!!!!!!!!!

//    $this->addFieldMapping('taxonomy_forums')->defaultValue(1);
    $this->addFieldMapping('taxonomy_forums', 'forum_id');
//      ->sourceMigration('PhpbbForums');                             //!!!!!!!!!!!!



    $this->addFieldMapping('taxonomy_forums:source_type')
      ->defaultValue('tid');
//    $this->addFieldMapping('hostname', 'poster_ip');
    if ($this->phpbbVersion == 3) {
      $this->addFieldMapping('status', 'post_approved');
    }
    else {
      $this->addFieldMapping('status')
        ->defaultValue(NODE_PUBLISHED);
    }
    $this->addFieldMapping('title', 'post_subject');
    $this->addFieldMapping('body', 'post_text');
    $this->addFieldMapping('created', 'post_time');
    $this->addFieldMapping('changed', 'post_edit_time');
    $this->addFieldMapping('language')->defaultValue($this->defaultLanguage);           //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    $this->addFieldMapping('sticky', 'topic_type');
    if (module_exists('statistics')) {
      $this->addFieldMapping('totalcount','topic_views');
      $this->addFieldMapping('daycount')
        ->defaultValue(0);
      $this->addUnmigratedDestinations(array('timestamp'));
    }
    $this->addFieldMapping(NULL, 'bbcode_uid')->issueGroup(t('DNM')); // Used to sanitise body text

    // Unmapped destination fields
    $this->addUnmigratedDestinations(array(
      'body:summary', 'body:format', 'body:language',
      'comment',
      'is_new',
      'log',
      'path',
      'pathauto',
      'promote',
      'revision',
      'revision_uid',
      'taxonomy_forums:create_term', 'taxonomy_forums:ignore_case',
      'tnid',
      'translate',
    ));

    $this->addUnmigratedSources(array(
      'post_edit_count',
      'topic_id',
    ));

    if ($this->phpbbVersion == 3) {
      $this->addUnmigratedSources(array(
        'post_edit_reason',
        'post_edit_user',
      ));

    }
  }

  /**
   * Method to return the query used in the import.
   * Can be called by sub classes and extended if needed.
   */
  protected function query() {
    $query = Database::getConnection('default', 'avrfreaks_phpbb_database_connection')->select('posts', 'p');

    $query->leftJoin('topics', 't', 'p.topic_id = t.topic_id');
    $query->fields('p', array(
      'forum_id', 'post_id', 'poster_id', //'poster_ip',
      'post_time', 'post_edit_time', 'post_edit_count'));
    $query->fields('t', array('topic_type','topic_views','topic_id'));
    $query->where('p.post_id = t.topic_first_post_id')
      ->orderBy('post_time');
    // post_approved introduced in phpBB 3
    if ($this->phpbbVersion == 3) {
      $query->addField('p', 'post_approved');
      $query->condition('post_approved', 1);
      $query->addField('p', 'post_subject');
      $query->addField('p', 'post_text');
      $query->addField('p', 'bbcode_uid');
      $query->addField('p', 'post_edit_reason');
      $query->addField('p', 'post_edit_user');
    }
    else {
      $query->innerJoin('posts_text', 'pt', 'p.post_id = pt.post_id');
      $query->addField('pt', 'post_subject');
      $query->addField('pt', 'post_text');
      $query->addField('pt', 'bbcode_uid');
    }

    return $query;
  }

  function prepareRow($post) {
    if (!$this->checkRowRequirements($post)) {
      return FALSE;
    }

    if (parent::prepareRow($post) === FALSE) {
      return FALSE;
    }

    $post->forum_id = $this->getNewForumId($post);

    self::$tags_matches = array();

    // remove the :bbcode_uid from post_text
    if (!empty($post->bbcode_uid)) {
      $post->post_text = PhpbbContentHelpers::stripUid($post->post_text, $post->bbcode_uid);
    }
    $post->post_text = PhpbbContentHelpers::stripBbcode($post->post_text);
    $post->post_text = PhpbbContentHelpers::textSanitise($post->post_text);
    $post->post_subject = PhpbbContentHelpers::textSanitise($post->post_subject);
    if ($post->post_edit_time == 0) {
      $post->post_edit_time = $post->post_time;
    }
    $post->topic_type = ($post->topic_type > 0) ? 1 : 0;
    $post->forum_id = ($post->forum_id == 0) ? 1 : $post->forum_id;
  }

  public function complete($node, stdClass $row) {
//    if (module_exists('phpbb_redirect')) {
//      phpbb_redirect_add_node($node->nid, $row->topic_id, $this->migration_settings['machine_name']); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//    }

    // Save a new revision if the post had been edited
    if ($this->phpbbVersion == 3 && $row->post_edit_count > 0) {
      $node->revision = TRUE;
      $node->log = $row->post_edit_reason;
      $original_user = $node->uid;
      node_save($node);
      // fixup the node object, modified should be from migrate.
      $node_query = db_update('node');
      $node_query->condition('nid',$node->nid);
      $node_query->fields(array('changed' => $row->post_edit_time,'uid' => $original_user));
      $node_query->execute();

      $node_revision_query = db_update('node_revision');
      $node_revision_query->condition('vid',$node->vid);
      $fields = array('timestamp' => $row->post_edit_time);
      // Apply the correct user to the revision. If not the original user.
      $account_id = $original_user;
      if ($row->post_edit_user != $row->poster_id) {
        $user_migration_name = $this->getMigrationName('User');
        $user_migration = MigrationBase::getInstance($user_migration_name);
        $source_row = $user_migration->map->getRowBySource(array($row->post_edit_user));
        $account_id = $source_row['destid1'];
      }
      $fields['uid'] = $account_id;
      $node_revision_query->fields($fields);
      $node_revision_query->execute();
    }
  }

  public function completeRollback($nid) {
//    if (module_exists('phpbb_redirect')) {
//      phpbb_redirect_remove_node($nid, $this->migration_settings['machine_name']);
//    }
  }

  function checkRowRequirements($post) {
    if (!self::$tags_preg_expr) {
      self::$tags_preg_expr = '#\b' . implode('|', array_keys(self::$forum_keywords_map)) . '\b#';
    }

    if (in_array($post->forum_id, array_keys(self::$forum_map))) {
      return TRUE;
    }

    if (preg_match_all(self::$tags_preg_expr, $post->post_subjects, $matches)
        || preg_match_all(self::$tags_preg_expr, $post->post_text, $matches)) {
      self::$tags_matches = $matches;
      return TRUE;
    }
  }

  private function getNewForumId($post) {
    if (self::$tags_matches) {
      foreach (array_keys(self::$forum_keywords_map) as $keyword) {
        if (in_array($keyword, self::$tags_matches)) {
          $new_forum_name = self::$forum_keywords_map[$keyword];
        }
      }
    }
    else {
      $new_forum_name = self::$forum_map[$post->forum_id];
    }

    $query = db_select('taxonomy_term_data', 't');
//    $query->leftJoin('taxonomy_vocabulary', 'v', 't.vid = v.vid');
    $result = $query->fields('t', array('tid'))
//      ->condition('v.machine_name', 'forum')
      ->condition('t.name', $new_forum_name)
      ->execute()
      ->fetchField();

    return $result;
  }
}
