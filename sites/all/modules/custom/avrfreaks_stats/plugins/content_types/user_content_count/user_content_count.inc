<?php

/**
 * @file
 * Plugin to handle Social Share options.
 */
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('AVR User Content - less than 5 posts'),
  'single' => TRUE,
  'category' => t('AvrFreaks'),
  'description' => t('AVR User Content - less than 5 posts'),
);

function avrfreaks_stats_user_content_count_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();

  $query = db_select('node', 'n');
  $query->fields('n', array('uid'));
  $query->addExpression('COUNT(n.nid)', 'posts');
  $query->havingCondition('posts', '5', '<')->groupBy('n.uid');
  $result = $query->execute()->fetchAll();
  
  $header = array('UID', 'POSTS');
 

  return $block;
}

/**
 * We need the form callback to allow title override.
 */
function avrfreaks_stats_user_content_count_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  return $form;
}

/**
 * Submit callback for configuration form.
 */
function avrfreaks_stats_user_content_count_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['values']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title.
 */
function avrfreaks_stats_user_content_count_content_type_admin_title($subtype, $conf) {
  return t('AVR User Content - less than 5 posts');
}