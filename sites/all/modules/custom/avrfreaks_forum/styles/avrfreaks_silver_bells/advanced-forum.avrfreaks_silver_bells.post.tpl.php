<?php

/**
 * @file
 *
 * Theme implementation: Template for each forum post whether node or comment.
 *
 * All variables available in node.tpl.php. In addition, AVRFreaks Forum makes available the following
 * variables:
 *
 * - $reply_link: Text link / button to reply to topic.
 * - $total_posts: Number of posts in topic (not counting first post).
 * - $new_posts: Number of new posts in topic, and link to first new.
 * - $links_array: Unformatted array of links.
 * - $account: User object of the post author.
 * - $name: User name of post author.
 * - $author_pane: Entire contents of the Author Pane template.
 */
?>
<?php if ($top_post): ?>
  <?php print $topic_header; ?>
<?php endif; ?>

<a id="forum-topic-top"></a>
<div class="forum-topic-hat clearfix">
  <div class="forum-hat-left-side">Author</div>
  <div class="forum-hat-right-side">Message</div>
</div>
<div id="<?php print $post_id; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="forum-post-info clearfix">
    <?php if (!$node->status): ?>
      <span class="unpublished-post-note"><?php print t("Unpublished topic") ?></span>
    <?php endif; ?>        
    <div class="forum-author-name-block">
      <?php if (!empty($avr_author_pane)): ?>
        <?php print $avr_author_pane; ?>
      <?php endif; ?>
    </div>
    <div class="forum-post-panel-sub">
      <div class="forum-post-user-rate">
        <?php if (!empty($user_rate)): ?>
          <?php print $user_rate; ?>
        <?php endif; ?>
      </div>
      <div class="forum-post-panel-joined">
        <?php if (!empty($join_date)): ?>
          <?php print $join_date; ?>
        <?php endif; ?>
      </div>
      <div class="forum-post-panel-posts">
        <?php if (!empty($user_posts)): ?>
          <?php print $user_posts; ?>
        <?php endif; ?>
      </div>
      <div class="forum-post-user-location">
        <?php if (!empty($user_location)): ?>
          <?php print $user_location; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="avr-forum-post-wrapper">
    <div class="avr-forum-post-panel-main clearfix">
      <div class="forum-post-upper-panel <?php print (isset($comment_flagged) && $comment_flagged) ? 'solved' : ''?>">
        <div class="avr-forum-posted-on">
          <div class="forum-post-number"><?php print $permalink; ?></div>
          <div class="forum-post-rating">
            <div class="post-date">
              <span class="label-text">Posted: </span>
              <?php print $post_date; ?>
              <?php if (!empty($new)): ?>
                <a id="new"><span class="new">(<?php print $new ?>)</span></a>
              <?php endif; ?>
              <?php if (!empty($in_reply_to)): ?>
                <span class="forum-in-reply-to"><?php print $in_reply_to; ?></span>
              <?php endif; ?>
            </div>
            <div class="rating-widget">
              <?php print render($content['rate_fivestar_widget']); ?>              
            </div>              
          </div>
        </div>
      </div>
        <div class="forum-post-content">
          <?php
            hide($content['taxonomy_forums']);
            hide($content['comments']);
            hide($content['links']);
            if (!$top_post)
              hide($content['body']);
            print render($content);
          ?>
        </div>
        <?php if (!empty($forum_poll)):?>
          <div class="forum-post-poll">
            <?php print $forum_poll; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($forum_tags)):?>
          <div class="post-tags">
            <?php print $forum_tags; ?>
          </div>
        <?php endif; ?>
        <?php if (!empty($signature)): ?>
          <div class="author-signature">
            <?php print $signature; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="forum-post-footer clearfix">
      <div class="forum-post-links">
        <?php print render($content['links']); ?>
          <a href="#forum-topic-top" title="<?php print t('Jump to top of page'); ?>" class="af-button-small"><span><?php print t("Top"); ?></span></a>
      </div>
    </div>
</div>
<?php print render($content['comments']); ?>
