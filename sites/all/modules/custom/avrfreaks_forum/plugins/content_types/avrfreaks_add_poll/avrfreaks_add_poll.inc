<?php

/**
 * @file
 * Plugin to handle Social Share options.
 */
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('AVR Add Poll'),
  'single' => TRUE,
  'category' => t('AvrFreaks'),
  'description' => t('Adds poll link to a specific forum topic'),
  'hook theme' => 'avrfreaks_forum_avrfreaks_add_poll_content_type_theme',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

function avrfreaks_forum_avrfreaks_add_poll_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();

  $node_data = $context->data;
  $destination = current_path();
  
  $build = array(
    '#theme' => 'avrfreaks_poll_link',
    '#poll_link' => (user_access('create poll content')) ? l(t('Attach a Poll'), 'node/add/poll', array('query' => array('nid' => $node_data->nid, 'destination' => $destination))): '',  
  );

  $block->content = array(
    'content' => $build,
  );


  return $block;
}

function avrfreaks_forum_avrfreaks_add_poll_content_type_theme(&$theme, $plugin) {
  $theme['avrfreaks_poll_link'] = array(
    'template' => 'avrfreaks-poll-link',
    'variables' => array(
      'poll_link' => NULL,
    ),
    'path' => $plugin['path'],
  );
}

/**
 * We need the form callback to allow title override.
 */
function avrfreaks_forum_avrfreaks_add_poll_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  return $form;
}

/**
 * Submit callback for configuration form.
 */
function avrfreaks_forum_avrfreaks_add_poll_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['values']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title.
 */
function avrfreaks_forum_avrfreaks_add_poll_content_type_admin_title($subtype, $conf) {
  return t('AVR Add Poll');
}