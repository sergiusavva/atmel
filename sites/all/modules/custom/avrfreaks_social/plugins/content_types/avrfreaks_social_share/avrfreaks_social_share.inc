<?php

/**
 * @file
 * Plugin to handle Social Share options.
 */
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('AVR Social Share'),
  'single' => TRUE,
  'category' => t('AvrFreaks'),
  'description' => t('Adds social share tool in the header'),
  'hook theme' => 'avrfreaks_social_share_avrfreaks_social_share_content_type_theme'
);

function avrfreaks_social_avrfreaks_social_share_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();

  drupal_add_js('jQuery(document).ready(function () { var switchTo5x=true; });', array('type' => 'inline', 'scope' => 'footer'));
  drupal_add_js('http://w.sharethis.com/button/buttons.js', array('scope' => 'footer'));
  drupal_add_js('jQuery(document).ready(function () { stLight.options({publisher: "2dfdc206-cbd7-46a6-bfeb-56c7481d5967", doNotHash: false, doNotCopy: false, hashAddressBar: false}); });', array('type' => 'inline', 'scope' => 'footer'));

  $front = drupal_is_front_page();
  
  $build = array(
    '#theme' => 'avrfreaks_social_share',
    '#front' => $front,
  );

  $block->content = array(
    'content' => $build,
  );


  return $block;
}

function avrfreaks_social_share_avrfreaks_social_share_content_type_theme(&$theme, $plugin) {
  $theme['avrfreaks_social_share'] = array(
    'template' => 'avrfreaks-social-share',
    'variables' => array(
      'front' => NULL,
    ),
    'path' => $plugin['path'],
  );
}

/**
 * We need the form callback to allow title override.
 */
function avrfreaks_social_avrfreaks_social_share_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  return $form;
}

/**
 * Submit callback for configuration form.
 */
function avrfreaks_social_avrfreaks_social_share_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['values']) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Returns the administrative title.
 */
function avrfreaks_social_avrfreaks_social_share_content_type_admin_title($subtype, $conf) {
  return t('AVR Social share');
}