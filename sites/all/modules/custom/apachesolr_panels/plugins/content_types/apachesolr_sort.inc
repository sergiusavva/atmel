<?php

if (module_exists('apachesolr_search')) {
  $plugin = array(
    'single' => TRUE,
    'no title override' => TRUE,
    'title' => t('Apache Solr search sort'),
    'icon' => 'icon_search.png',
    'description' => t('Sort results of an Apache Solr search for Samsung.'),
    'required context' => new ctools_context_optional(t('Context to fetch search query from'), 'string'),
    'render callback' => 'apachesolr_panels_apachesolr_sort_content_type_render',
    'category' => t('Apache Solr Search'),
    'defaults' => array(),
    'render first' => TRUE,
  );
}

/**
 * @param $subtype
 * @param $conf
 * @param $args
 * @param $context
 * @return array|null
 */
function apachesolr_panels_apachesolr_sort_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();
  $block->module = 'apachesolr_panels';
  $block->delta = 'sort';
  $block->content = '';


  $environments = apachesolr_load_all_environments();
  foreach ($environments as $env_id => $environment) {
    if (apachesolr_has_searched($env_id) && !apachesolr_suppress_blocks($env_id)) {
      $response = NULL;
      $query = apachesolr_current_query($env_id);
      if ($query) {
        // Get the query and response. Without these no blocks make sense.
        $response = apachesolr_static_response_cache($query->getSearcher());
      }
      if (empty($response) || ($response->response->numFound < 2)) {
        return NULL;
      }

      $sorts = $query->getAvailableSorts();

      // Show only custom sort
      $custom_sort = array('score', 'ds_created');
      $sorts = array_intersect_key($sorts, array_flip($custom_sort));

      if (empty($sorts)) {
        return $block;
      }
      $block->title = t('Sort By:');

      // Rewrite sort title.
      $sorts['score']['title'] = t('Most Relevant');
      $sorts['ds_created']['title'] = t('Most Recent');

      // Get the current sort as an array.
      $solrsort = $query->getSolrsort();

      $sort_links = array();
      $path = $query->getPath();
      $new_query = clone $query;
      $toggle = array('asc' => 'desc', 'desc' => 'asc');
      foreach ($sorts as $name => $sort) {
        $active = $solrsort['#name'] == $name;
        if ($name == 'score') {
          $direction = '';
          $new_direction = 'desc'; // We only sort by descending score.
        }
        elseif ($active) {
          $direction = $toggle[$solrsort['#direction']];
          $new_direction = $toggle[$solrsort['#direction']];
        }
        else {
          $direction = '';
          $new_direction = $sort['default'];
        }
        $new_query->setSolrsort($name, $new_direction);
        $sort_links[$name] = array(
          'text' => $sort['title'],
          'path' => $path,
          'options' => array('query' => $new_query->getSolrsortUrlQuery()),
          'active' => $active,
          'direction' => $direction,
        );
      }

      foreach ($sort_links as $name => $link) {
        $themed_links[$name] = theme('apachesolr_sort_link', $link);
      }

      $block->content = theme('apachesolr_sort_list', array('items' => $themed_links));
      return $block;
    }
  }
}