<?php

/**
 * @file
 * Provides breadcrumb formatting for the site.
 */

/**
 * Implements hook_init().
 */
function avrfreaks_breadcrumbs_init() {
  // We don't need to do anything for the home page, and the homepage
  // is heavy enough, so we'll bail out early here.
  if (drupal_is_front_page()) {
    return;
  }

  // Start the breadcrumb array.
  $breadcrumb = array();
  $breadcrumb[] = l('Home', '<front>');
  
  // Try to get the current object for later use.
  $object = menu_get_object();

  // Forums
  if (arg(0) == 'forum') {
    $breadcrumb[] = l('Forums', 'forum');
  }
  if (preg_match('/forum\/(\d*)/', $_GET['q'], $matches)) {
    $term = taxonomy_term_load($matches[1]);
    if (isset($term->parents[1])) {
      $breadcrumb[] = l($term->parents[1]->name, 'taxonomy/term/' . $term->parents[1]->tid);
    }
    $breadcrumb[] = $term->name;
  }

  // Forum discussion
  if (is_object($object) && $object->type == 'forum') {
    // Something else is setting this breadcrumb
  }

  // Projects
  if (arg(0) == 'projects') {
    $breadcrumb[] = l('Projects', 'projects');
  }
  // Project pages
  if (is_object($object) && $object->type == 'project') {
    $breadcrumb[] = l('Projects', 'projects');
    $breadcrumb[] = $object->title;
  }
  
  // Vendors
  if (arg(0) == 'vendors') {
    $breadcrumb[] = l('Vendors', 'vendors');
  }
  // Vendor profiles
  if (is_object($object) && $object->type == 'vendor') {
    $breadcrumb[] = l('Vendors', 'vendors');
    $breadcrumb[] = $object->title;
  }

  // Wiki
  if (drupal_get_path_alias($_GET['q']) == 'wiki') {
    $breadcrumb[] = l('Wiki', 'wiki');
  } else {
    if (is_object($object) && $object->type == 'wiki') {
      $breadcrumb[] = l('Wiki', 'wiki');
      $breadcrumb[] = l($object->title, 'node/' . $object->nid);
    }
  }
  if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_term_load(arg(2));
    if ($term->vocabulary_machine_name == 'wiki_categories') {
      $breadcrumb[] = l('Wiki', 'wiki');
      $breadcrumb[] = l($term->name, 'taxonomy/term/' . $term->tid);
    }
  }

  // User profile pages
  if (arg(0) == 'user') {

    // Anon paths
    if (arg(1) == 'password') {
      $breadcrumb[] = t('Request new password');
    }
    if (arg(1) == 'register') {
      $breadcrumb[] = t('Sign up');
    }

    global $user;
    if ($user->uid === 0) {
      $breadcrumb[] = t('User login');
    }
    else {
      if (arg(1) === $user->uid || !arg(1)) {
        $breadcrumb[] = l('My account', 'user');
      }
      else {
        $account = user_load(arg(1));
        $breadcrumb[] = l($account->name, 'user/' . $account->uid);
      }

      if (arg(2) === 'edit-profile') {
        $breadcrumb[] = t('Edit profile');
      }
      if (arg(2) === 'dashboard') {
        $breadcrumb[] = t('Dashboard');
      }
      if (arg(2) === 'edit') {
        $breadcrumb[] = t('Edit account');
      }
      if (arg(2) === 'hybridauth') {
        $breadcrumb[] = t('HybridAuth');
      }
      if (arg(2) === 'merge-account') {
        $breadcrumb[] = t('Merge account');
      }
      if (arg(2) === 'messages') {
        $breadcrumb[] = t('Messages');
      }
      if (arg(2) === 'notification-settings') {
        $breadcrumb[] = t('Notification settings');
      }
      if (arg(2) === 'shortcuts') {
        $breadcrumb[] = t('Shortcuts');
      }
    }
  }

  drupal_set_breadcrumb($breadcrumb);
}
