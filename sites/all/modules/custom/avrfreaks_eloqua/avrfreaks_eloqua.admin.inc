<?php

/**
 * Admin settings form of module.
 */
function avrfreaks_eloqua_settings($form, &$form_state) {
  $form = array();

  $form['wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Scripts.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  
  $form['wrapper']['eloqua_tracking_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Eloqua tracking script'),
    '#default_value' => variable_get('eloqua_tracking_script'),
  );

  $form['wrapper']['demand_base_tracking_script'] = array(
    '#type' => 'textarea',
    '#title' => t('Demand base tracking script (for anonymous users only)'),
    '#default_value' => variable_get('demand_base_tracking_script'),
  );

  $form['add_on_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add javascript on admin pages'),
    '#default_value' => variable_get('add_on_admin', 0),
  );

  return system_settings_form($form);
}