<?php

/**
 * Access plugin that provides property based access control.
 */
class avrfreaks_rewrites_access_plugin extends views_plugin_access {

  function summary_title() {
    return t('Hide bookmarks tab for !current_user');
  }

  function access($account) {
    return avrfreaks_rewrites_bookmarks_access();
  }

  function get_access_callback() {
    return array('avrfreaks_rewrites_bookmarks_access', array());
  }
}