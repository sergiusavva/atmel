<?php

/**
 * @file
 * Definition of avrfreaks_rewrites_last_node_comment.
 */

/**
 * Description of what my handler does.
 */
class avrfreaks_rewrites_last_node_comment extends views_handler_field {
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array(
      'table' => 'node',
      'field' => 'nid'
    );
    $this->additional_fields['node_type'] = array(
      'table' => 'node',
      'field' => 'type'
    );
    $this->additional_fields['comment_count'] = 'comment_count';
    $this->additional_fields['cid'] = 'cid';
    $this->additional_fields['last_comment_timestamp'] = 'last_comment_timestamp';
  }

  /**
   * Loads additional fields.
   */
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Default options form.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['custom_class'] = array('default' => '');

    return $options;
  }

  /**
   * Creates the form item for the options added.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['custom_class'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional class'),
      '#default_value' => empty($this->options['custom_class']) ? '' : $this->options['custom_class'],
      '#description' => t('Classes for phone link.'),
      '#weight' => -10,
    );
  }

  /**
   * Renders the field handler.
   */
  function render($values) {
    $nid = $this->get_value($values, 'nid');
    $type = $this->get_value($values, 'node_type');
    $count = $this->get_value($values, 'comment_count');
    $cid = $this->get_value($values, 'cid');

    $time = $this->get_value($values, 'last_comment_timestamp');
    $time = format_date($time, 'long_post_date');

    $last_page = $this->get_page($type, $count);
    $options = array('fragment' => "comment-$cid");

    if (!empty($this->options['custom_class'])) {
      $options['attributes'] = array('class' => trim($this->options['custom_class']));
    }

    if ((bool) $count) {
      if ($last_page > 0) {
        $options['query'] = array('page' => $last_page);
      }
      return l(t('Last commented on !time', array('!time' => $time)), "node/$nid", $options);
    }
    return NULL;

  }

  /**
   * Returns the page number of the last page starting at 0 like the pager does.
   * @param $node_type
   *  A node type as 'form', article..
   * @param $comment_count
   *  The number of comments for some node.
   * @return int
   */
  private function get_page($node_type, $comment_count) {
    $comments_per_page = variable_get('comment_default_per_page_' . $node_type, 50);
    $comment_count = isset($comment_count) ? $comment_count : 0;
    return ceil($comment_count / $comments_per_page) - 1;
  }
}
