<?php

/**
 * @file
 * Definition of avrfreaks_rewrites_last_node_comment.
 */

/**
 * Description of what my handler does.
 */
class avrfreaks_rewrites_last_combine_body extends views_handler_field {
  /**
   * Add some required fields needed on render().
   */
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = array(
      'table' => 'node',
      'field' => 'nid'
    );
  }

  /**
   * Loads additional fields.
   */
  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  /**
   * Renders the field handler.
   */
  function render($values) {
//    dpm($values);
//    return parent::render($values);
//    $nid = $this->get_value($values, 'nid');
//    $type = $this->get_value($values, 'node_type');
//    $count = $this->get_value($values, 'comment_count');
//    $cid = $this->get_value($values, 'cid');
//
//    $time = $this->get_value($values, 'last_comment_timestamp');
//    $time = format_date($time, 'long_post_date');
//
//    $last_page = $this->get_page($type, $count);
//    $options = array('fragment' => "comment-$cid");
//
//    if (!empty($this->options['custom_class'])) {
//      $options['attributes'] = array('class' => trim($this->options['custom_class']));
//    }
//
//    if ((bool) $count) {
//      if ($last_page > 0) {
//        $options['query'] = array('page' => $last_page);
//      }
//      return l(t('Last commented on !time', array('!time' => $time)), "node/$nid", $options);
//    }
    return NULL;

  }
}
