<?php
/**
 * @file
 * Views definitions for rx_program_list module.
 */

/**
 * Implements hook_views_data().
 */
function avrfreaks_rewrites_views_data() {
  $data = array();
  $data['node_comment_statistics']['last_comment_link'] = array(
    'title' => t('Link to last comment'),
    'help' => t('Display link to last node comment.'),
    'field' => array(
      'handler' => 'avrfreaks_rewrites_last_node_comment',
    ),
  );

  $data['comment']['combine_body_comment_node'] = array(
    'title' => t('Combine body from node and comment'),
    'help' => t('Display last comment, if no comment display node body.'),
    'field' => array(
      'handler' => 'avrfreaks_rewrites_last_combine_body',
    ),
  );
  return $data;
}
