<?php
/**
 * @file
 * API Index.
 */

/**
 * Implements hook_bbcode_quote_filters_alter().
 */
function hook_bbcode_quote_filters_alter() {
  $options['convert_double_less_than_sign'] = array(
    'pattern' => '/(<<)/',
    'replacement' => array(
      'html' => '<<',
      'callback' => 'bbcode_quote_convert_double_less_than_sign',
    ),
  );
}

/**
 * Replacement callback.
 */
function bbcode_quote_convert_double_less_than_sign($pattern, $replacement, $string) {
  $string = preg_replace($pattern, htmlentities($replacement), $string);
  return $string;
}
